# OpenML dataset: qualitative-bankruptcy

https://www.openml.org/d/1495

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:  A. Martin, J. Uthayakumar, M. Nadarajan, V. Prasanna Venkatesan   
**Source**: UCI   
**Please cite**:    

* Abstract: 

Predict the Bankruptcy from Qualitative parameters from experts.

* Source:

Source Information
-- Creator : Mr.A.Martin(jayamartin '@' yahoo.com)
Mr.J.Uthayakumar (uthayakumar17691 '@' gmail.com)
Mr.M.Nadarajan(nadaraj.muthuvel '@' gmail.com)
-- Guided By : Dr.V.Prasanna Venkatesan
-- Institution : Sri Manakula Vinayagar Engineering College and Pondicherry University
-- Country : India
-- Date : February 2014


* Data Set Information:

The parameters which we used for collecting the dataset is referred from the paper 'The discovery of expert' decision rules from qualitative bankruptcy data using genetic algorithms' by Myoung-Jong Kim*, Ingoo Han.


* Attribute Information: 
(P=Positive,A-Average,N-negative,B-Bankruptcy,NB-Non-Bankruptcy) 

1. Industrial Risk: {P,A,N} 
2. Management Risk: {P,A,N} 
3. Financial Flexibility: {P,A,N} 
4. Credibility: {P,A,N} 
5. Competitiveness: {P,A,N} 
6. Operating Risk: {P,A,N} 
7. Class: {B,NB}


* Relevant Papers:

The parameters which we used for collecting the dataset is referred from the paper 'The discovery of expertsâ€™ decision rules from qualitative bankruptcy data using genetic algorithms' by Myoung-Jong Kim*, Ingoo Han.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1495) of an [OpenML dataset](https://www.openml.org/d/1495). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1495/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1495/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1495/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

